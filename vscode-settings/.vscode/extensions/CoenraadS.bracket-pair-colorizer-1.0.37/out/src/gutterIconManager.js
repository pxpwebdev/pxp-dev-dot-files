"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_uri_1 = require("vscode-uri");
class GutterIconManager {
    constructor() {
        this.fs = require("fs");
        this.escape = require("escape-html");
        this.iconDict = new Map();
        this.disposables = new Array();
    }
    Dispose() {
        this.disposables.forEach((callback) => {
            callback();
        });
        this.disposables = [];
    }
    GetIconUri(bracket, color, fontFamily) {
        const colorDict = this.iconDict.get(bracket);
        if (colorDict) {
            const uri = colorDict.get(color);
            if (uri) {
                return uri;
            }
            else {
                const newUri = this.createIcon(color, bracket, fontFamily);
                colorDict.set(color, newUri);
                return newUri;
            }
        }
        else {
            const newUri = this.createIcon(color, bracket, fontFamily);
            const dict = new Map();
            dict.set(color, newUri);
            this.iconDict.set(bracket, dict);
            return newUri;
        }
    }
    createIcon(color, bracket, fontFamily) {
        const svg = `<svg xmlns="http://www.w3.org/2000/svg" height="10" width="10">` +
            `<text x="50%" y="50%" fill="${color}" font-family="${fontFamily}" font-size="10" ` +
            `text-anchor="middle" dominant-baseline="middle">` +
            `${this.escape(bracket)}` +
            `</text>` +
            `</svg>`;
        const encodedSVG = encodeURIComponent(svg);
        const URI = "data:image/svg+xml;utf8," + encodedSVG;
        return vscode_uri_1.default.parse(URI);
    }
}
exports.default = GutterIconManager;
//# sourceMappingURL=gutterIconManager.js.map